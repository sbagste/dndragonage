loadScript('/dndragonage/components/header.js');
loadScript('/dndragonage/components/footer.js');
loadScript('/dndragonage/components/navbar.js');

function loadScript(url) {    
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = url;
  head.appendChild(script);
}

function displayNavbar() {
  const nav = document.getElementsByClassName("nav-root");
  if (nav[0].style.display === "block") {
    nav[0].style.display = "none";
  } else {
    foldSubNavigationLinks();
    nav[0].style.display = "block";
  }
}

function displaySubNavigationLinks(id) {
  const navElement = document.getElementById(id);
  if (navElement.style.display === "block") {
    navElement.style.display = "none";
  } else {
    foldSubNavigationLinks();
    navElement.style.display = "block";
  }
}

function foldSubNavigationLinks() {
  const subNavigationLinks = document.getElementsByClassName("nav-links")  
  Array.prototype.forEach.call(subNavigationLinks, function(link) {
    link.style.display = "none";
  });
}