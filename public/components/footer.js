class Footer extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `
      <footer>
        <div class="is-page">
            <small>Thrown by <a href="https://docs.gitlab.com/ee/user/project/pages/">GitLab Pages</a></small>
        </div>
      </footer>
      `;
    }
  }
  
  customElements.define('footer-component', Footer);