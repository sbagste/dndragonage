class Navbar extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `
        <nav class="nav-root">
          <div class="is-page">
            <!-- First Row -->
            <ul class="nav-list">
              <li onclick="displaySubNavigationLinks('heritages-links')">Héritages</li>
              <li onclick="displaySubNavigationLinks('classes-links')">Classes</li>
              <li onclick="displaySubNavigationLinks('codex-links')">Codex</li>
            </ul>
            <!-- Legacies -->
            <ul class="nav-links" id="heritages-links">
              <li><a href="/dndragonage/heritages/demi-elfe">Demi-Elfe</a></li>
              <li><a href="/dndragonage/heritages/elfe">Elfe</a></li>
              <li><a href="/dndragonage/heritages/humain">Humain</a></li>
              <li><a href="/dndragonage/heritages/qunari">Qunari</a></li>
              <li><a href="/dndragonage/heritages/nain">Nain</a></li>
            </ul>  
            <!-- Classes -->
            <ul class="nav-links" id="classes-links">
              <li><a href="/dndragonage/classes/archiviste">Archiviste</a></li>
              <li><a href="/dndragonage/classes/guerrier">Guerrier</a></li>
              <li><a href="/dndragonage/classes/guerrier-primal">Guerrier Primal</a></li>
              <li><a href="/dndragonage/classes/laetan">Laetan</a></li>
              <li><a href="/dndragonage/classes/mage-du-cercle">Mage du cercle</a></li>
              <li><a href="/dndragonage/classes/maleficar">Maleficar</a></li>
              <li><a href="/dndragonage/classes/rodeur">Rôdeur</a></li>
              <li><a href="/dndragonage/classes/roublard">Roublard</a></li>
              <li><a href="/dndragonage/classes/sorceleur">Sorceleur</a></li>
              <li><a href="/dndragonage/classes/templier">Templier</a></li>
            </ul> 
            <!-- Codex -->
            <ul class="nav-links" id="codex-links">
              <li><a href="/dndragonage/codex/armes">Armes</a></li>
              <li><a href="/dndragonage/codex/creatures">Créatures</a></li>
              <li><a href="/dndragonage/codex/degats">Dégâts</a></li>
              <li><a href="/dndragonage/codex/langues">Langues</a></li>
              <li><a href="/dndragonage/codex/sorts">Sorts</a></li>
            </ul>   
          </div>

        </nav>
      `;
    }
  }
  
  customElements.define('navbar-component', Navbar);
  