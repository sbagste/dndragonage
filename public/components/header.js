class Header extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `
      <header>
        <div class="is-page container">
          <a href="/dndragonage" class="is-discrete">
            <div>
              <h1>D&Dragon Age</h1>
              <div class="subtitle">Parcourez Thédas grâce à Donjons et Dragons 5e</div>
            </div>
          </a>
          <div><img src = "/dndragonage/assets/ancient_dragon.svg" class="is-red" id="dragon-banner" alt="Un ancien dragon"/></div>

          <a class="burger-menu" href="javascript:void(0);" onclick="displayNavbar()">
          <span class="burger-icon">
            <span></span>
            <span></span>
            <span></span>
          </span>
          </a>
        </div>
      </header>
      `;
    }
  }
  
  customElements.define('header-component', Header);
  